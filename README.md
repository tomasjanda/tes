# TES

Lectures and supplement material for Theory of Reliability course (TES).

Topic overview:
1. Basic relations and terminology
2. Common probability distributions and useful inequalities.
3. Transformation of random variables.
4. Reliability of simple structures
5. Evolution of reliability in time
6. Solution methods and models
7. Markov chains and Kolmogorov equations
8. Reliability in design codes
9. First order reliability methods Transformation
10. Monte Carlo method, Latin Hypercube sampling
11. Advanced methods: Subset simulation, MCMC sampling
12. Bayesian inference, Metropolis Hastings algorithm
