{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Relability of systems\n",
    "\n",
    "Element:\n",
    "- $A_i$ ... $i$-th elment is failed\n",
    "- $\\overline{A}_i$ ... $i$-th elment is working\n",
    "- $P_{f,i}=\\Pr(A_i)$ ... probability that $i$-th element fails\n",
    "- $Re_i=\\Pr(\\overline{A}_i)=1-\\Pr(A_i)$ ... reliability of $i$-th element\n",
    "\n",
    "System:\n",
    "- $A$ ... system is failed at time $t$\n",
    "- $\\overline{A}$ ... system is working\n",
    "- $P_f=\\Pr(A)$ ... probability of system failure\n",
    "- $Re=\\Pr(\\overline{A})=1-\\Pr(A)$ ... reliability of system\n",
    "\n",
    "Union of $n$ events (events are sets)\n",
    "$$\\bigcup\\limits_{i=1}^{n} A_{i} = A_1 \\cup A_2 \\cup \\dots \\cup A_n$$\n",
    "\n",
    "Intersection of $n$ events\n",
    "$$\\bigcap\\limits_{i=1}^{n} A_{i} = A_1 \\cap A_2 \\cap \\dots \\cap A_n$$\n",
    "\n",
    "De Morgan's laws for two events\n",
    "$$A_1 \\cup A_2 = \\overline{\\overline{A_1}\\cap\\overline{A_2}}$$\n",
    "$$A_1 \\cap A_2 = \\overline{\\overline{A_1}\\cup\\overline{A_2}}$$\n",
    "\n",
    "De Morgan's laws for $n$ events\n",
    "$$\\bigcup\\limits_{i=1}^{n} A_{i} = \\overline{\\bigcap\\limits_{i=1}^{n} \\overline{A_{i}}}$$\n",
    "$$\\bigcap\\limits_{i=1}^{n} A_{i} = \\overline{\\bigcup\\limits_{i=1}^{n} \\overline{A_{i}}}$$\n",
    "\n",
    "Inclusion-exclusion principle for two events\n",
    "$$\\Pr(A\\cup B) = \\Pr(A) + \\Pr(B) - \\Pr(A \\cap B)$$\n",
    "\n",
    "Inclusion-exclusion principle for three events\n",
    "\\begin{align}\n",
    "\\Pr(A\\cup B \\cup C) &= \\Pr(A) + \\Pr(B) + \\Pr(C) \\\\\n",
    "&-\\Pr(A \\cap B)-\\Pr(A \\cap C)-\\Pr(B \\cap C) \\\\\n",
    "& + \\Pr(A \\cap B \\cap C)\n",
    "\\end{align}\n",
    "\n",
    "Inclusion-exclusion principle for four events\n",
    "\\begin{align}\n",
    "\\Pr(A\\cup B \\cup C \\cup D) &= \\Pr(A) + \\Pr(B) + \\Pr(C) + \\Pr(D) \\\\\n",
    "&-\\Pr(A \\cap B)-\\Pr(A \\cap C)-\\Pr(A \\cap D)-\\Pr(B \\cap C)-\\Pr(B \\cap D)-\\Pr(C \\cap D) \\\\\n",
    "& + \\Pr(A \\cap B \\cap C)+ \\Pr(A \\cap B \\cap D)+ \\Pr(A \\cap C \\cap D)+ \\Pr(B \\cap C \\cap D)\\\\\n",
    "& - \\Pr(A \\cap B \\cap C \\cap D)\n",
    "\\end{align}\n",
    "and so on, see https://en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle#In_probability\n",
    "\n",
    "\n",
    "Law of total probability ($B_i$ are pairwise disjoint events whose union is the entire sample space)\n",
    "$$\\Pr(A) = \\sum_{i=1}^n\\Pr(A \\cap B_i)$$ \n",
    "\n",
    "This is often used for special case of just two events $B_i$: some event $B$ and its complement $\\overline{B}$\n",
    "$$\\Pr(A) = \\Pr(A \\cap B)+\\Pr(A \\cap \\overline{B})$$\n",
    "\n",
    "Conditional probability\n",
    "$$\\Pr(A\\mid B) = \\frac{\\Pr(A \\cap B)}{\\Pr(B)}$$\n",
    "\n",
    "Only for _independent_ events $A$ and $B$ it holds that $\\Pr(A\\mid B) = \\Pr(A)$ and therefore from the above\n",
    "$$\\Pr(A \\cap B)=\\Pr(A)\\Pr(B)$$\n",
    "\n",
    "Probability of intersection of many _independent_ events is\n",
    "$$\\Pr\\left(\\bigcap\\limits_{i=1}^{n} A_{i}\\right) = \\prod_{i=1}^n\\Pr(A_i)$$\n",
    "\n",
    "It holds\n",
    "$$\\Pr(A) = 1-\\Pr(\\overline{A})$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Context\n",
    "We want to compute probability of failure of a system which is composed of several elements. We assume that we know the probability of failure of individual element. If the failures of individual systems are not independent we also need to know probability of simultaneous failure of any two (three, four, ... and all) elements.\n",
    "\n",
    "This means that our known input is\n",
    "$$\\Pr(A_i) \\text{ for } i=1\\dots n$$\n",
    "and also\n",
    "$$\\Pr(A_i \\cap A_j) \\text{ for } i<j$$\n",
    "and \n",
    "$$\\Pr(A_i \\cap A_j \\cap A_k) \\text{ for } i<j<k$$\n",
    "and so on. For $n$ dependent events we need $2^n-1$ values. Think of (hyper)cube cut into half in each direction. Or think of Venn diagram.\n",
    "\n",
    "For example for two events the individual pobabilities and their dependence is given by $\\Pr(A\\cap B)$, $\\Pr(A\\cap \\overline{B})$, $\\Pr(\\overline{A}\\cap B)$. Nevertheless, this is seldom known.\n",
    "\n",
    "More common is the knowledge of $\\Pr(A)$, $\\Pr(B)$, $\\Pr(A\\cap B)$.\n",
    "\n",
    "For independent events we do not need the probabilities of various intersections because \n",
    "$$\\Pr(A \\cap B) = \\Pr(A)\\Pr(B)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Serial system\n",
    "Failure of any element causes the system to fail.\n",
    "$$A = \\bigcup\\limits_{i=1}^{n} A_{i}$$\n",
    "Just apply inclusion-exclusion principle and get\n",
    "\\begin{align}\n",
    "\\Pr(A) &= \\Pr(A_1) + \\Pr(A_2) + \\dots + \\Pr(A_n)\\\\\n",
    "&-\\Pr(A_1\\cap A_2) - \\Pr(A_1\\cap A_3) - \\dots - \\Pr(A_{n-1}\\cap A_n)\\\\\n",
    "&+\\Pr(A_1\\cap A_2\\cap A_3) + \\dots\\\\\n",
    "& \\dots \\\\\n",
    "& \\pm \\Pr(A_1\\cap A_2\\cap \\dots \\cap A_n)\n",
    "\\end{align}\n",
    "\n",
    "In case of independent events it is usefull to use De Morgan's law\n",
    "$$A = \\bigcup\\limits_{i=1}^{n} A_{i}=\\overline{\\bigcap\\limits_{i=1}^{n} \\overline{A_{i}}}$$\n",
    "\n",
    "Then\n",
    "$$\\Pr(A) = \\Pr\\left(\\overline{\\bigcap\\limits_{i=1}^{n} \\overline{A_{i}}}\\right)=1-\\Pr\\left(\\bigcap\\limits_{i=1}^{n} \\overline{A_{i}}\\right) =1-\\prod_{i=1}^n\\Pr(\\overline{A_{i}}) =1-\\prod_{i=1}^n(1-\\Pr(A_i))\n",
    "$$\n",
    "\n",
    "This can be seen as\n",
    "$$Re = \\prod_{i=1}^n Re_i$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parallel system\n",
    "Only failure of all elements cause the system to fail.\n",
    "$$A = \\bigcap\\limits_{i=1}^{n} A_{i}$$\n",
    "\n",
    "For independent events we have\n",
    "$$\\Pr(A) = \\prod_{i=1}^n\\Pr(A_i)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two parallel elements connectet serially with third element\n",
    "$$A = (A_1 \\cap A_2)\\cup A_3$$\n",
    "\n",
    "Inclusion-exclucion principle\n",
    "$$\\Pr(A) = \\Pr((A_1 \\cap A_2)\\cup A_3) = \\Pr(A_1 \\cap A_2) + \\Pr(A_3) - \\Pr(A_1 \\cap A_2\\cap A_3)$$\n",
    "\n",
    "For independent events (serial connection of two elements\n",
    "$$\\Pr(A) =1-(1-\\Pr(A_1)\\Pr(A_2))(1-\\Pr(A_3))$$\n",
    "\n",
    "To verify it, manipulate the above to get\n",
    "$$\\Pr(A) =\\Pr(A_1)\\Pr(A_2) + \\Pr(A_3) - \\Pr(A_1)\\Pr(A_2)\\Pr(A_3)$$\n",
    "which correspon to the expression for dependent events above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two serial elements connectet parallelly with third element\n",
    "Do this as an excercise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Something that can not be decomposed into serial and parallel subsystems\n",
    "\n",
    "<img src=\"./key_element.png\" title=\"System with key element C\" style=\"width: 250px\" align=\"center\"/>\n",
    "\n",
    "We chose \"key element\". This is the element $C$ in our case.\n",
    "\n",
    "Failure of entire system $F$ is\n",
    "$$F = (F_{I} \\cap C) \\cup (F_{II} \\cap \\overline{C})$$\n",
    "where\n",
    "$$F_I = (A\\cup B)\\cap(D\\cup E)$$\n",
    "and \n",
    "$$F_{II} = (A\\cap B)\\cup(D\\cap E)$$\n",
    "\n",
    "\\begin{align}\n",
    "\\Pr(F) &= \\Pr(((A\\cup B)\\cap(D\\cup E) \\cap C) \\cup (((A\\cap B)\\cup(D\\cap E))\\cap \\overline{C}))\n",
    "\\end{align}\n",
    "\n",
    "\n",
    "Combining this we get\n",
    "\\begin{align}\n",
    "\\Pr(F) &= \\Pr(A\\cap D) + \\Pr(B\\cap E) + \\Pr(A\\cap E\\cap C) + \\Pr(B\\cap D\\cap C)\\\\\n",
    "& - \\Pr(A \\cap B \\cap D \\cap C)- \\Pr(A \\cap B \\cap E \\cap C)- \\Pr(A \\cap D \\cap E \\cap C)- \\Pr(B \\cap D \\cap E \\cap C)\\\\\n",
    "& -\\Pr(A \\cap B \\cap D \\cap E) + 2\\Pr(A \\cap B \\cap D \\cap E \\cap C)\n",
    "\\end{align}\n",
    "\n",
    "Or for independent events we directly write\n",
    "\\begin{align}\n",
    "\\Pr(F) &= \\Pr(F\\mid C)\\Pr(C) + \\Pr(F\\mid \\overline{C})\\Pr(\\overline{C})\\\\\n",
    "&= (1-(1-\\Pr(A))(1-\\Pr(B)))(1-(1-\\Pr(D))(1-\\Pr(E)))\\Pr(C)\\\\\n",
    "&+(1-(1-\\Pr(A)\\Pr(D))(1-\\Pr(B)\\Pr(E)))(1-\\Pr(C))\n",
    "\\end{align}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$F = (A \\cap B) \\cup (A \\cap C) \\cup (B \\cap C) \\cup (A \\cap B \\cap C)$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
