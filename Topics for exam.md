# Topics for TES exam

1. Reliability, probability of failure, performance function.
0. Probability, sample space vs. event space.
0. Probability of union of two or more events. What if events are exclusive?
0. Conditional probability. How to compute it? Example?
0. Law of total probability.
0. De Morgan's laws.
0. Chain rule of Probability.
0. Bayes' theorem. Example.
0. Discrete random variables. Probability mass function. Cumulative distribution function.
0. Continuous random variables. Probability density function. Cumulative distribution function.
0. Mean value. Variance. Covariance. Correlation coefficient.
0. Uniform distribution. PDF. CDF. Parameters.
0. Normal distribution. PDF. CDF. Parameters. 68-95-99.7 rule.
0. Lognormal distribution.
0. Transformation of single random variable.
0. Random variable as a function of two random variables. Sum of two random variables.
0. Special case of performance function: Z=R-E. Probability of failure.
0. Time evolution of reliability. Time-dependent failure rate. Constant failure rate.
0. Reliability of serial system.
0. Reliability of parallel system.
0. Combination of serial and parallel systems.
0. Key element approach to reliability of complex system.
0. Systems with multiple states. Markov models. Markov property. Transition matrix.
0. System with two states (operation vs. failure). Failure rate, repair rate. Evolution of probability of failure.
0. First order reliability method. Assumptions. Limitations.
0. Monte-Carlo methods. Crude MC, advantages, disadvantages.
0. Rejection sampling.
0. Latin hypercube sampling.
0. Subset simulation. Principle. Comparison to naive M-C method.
0. Markov chain Monte-Carlo sampling.
0. Bayesian inference.

_You can use any source of information during exam, including your notes, the notebooks and presentations in this repository or information from the internet, etc. Nevertheless, an assistance from other persons is not allowed._
