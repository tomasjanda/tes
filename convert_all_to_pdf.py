from os import system, listdir
from shutil import move
from os import mkdir, getcwd


def jup2pdf():
    print("Please input a run_no ")
    print("Only integers allowed")
    run_no = int(input())
    run_dir_name = 'pdf' + str(run_no)

    mkdir(run_dir_name)

    other_stuff = []
    for f in listdir("."):
        if f.endswith("ipynb"):
            system("jupyter nbconvert --to pdf " + f)
            # convert to pdf
            f_pdf = '.'.join([f[: -6], 'pdf'])
            # make the corresponding pdf file name
            move(f_pdf, run_dir_name, )
            # move the corresponding pdf file
        else:
            print('You have something other than a Jupyter notebook.')
            # print(f)
            other_stuff.append(f)

    print('The following pdf files have been created.')
    print('in', run_dir_name)
    print()
    for f in listdir(run_dir_name):
        if f.endswith("pdf"):
            print(f)
        else:
            print('PDFs not found in ', getcwd())

    print('We also found the following')
    for other_item in other_stuff:
        print(other_item)

    return None


'''

'''
check_pres_of = [f.endswith("ipynb") for f in listdir(".")]

if (True in check_pres_of):
    print('Found Jupyter notebooks in', getcwd())
    jup2pdf()
    # print([f.endswith("ipynb") for f in listdir(".")])
else:
    print('Jupyter notebooks not found in ', getcwd())
